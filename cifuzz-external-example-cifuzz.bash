#!/bin/bash

export WORKDIR=/tmp/cifuzz-workdir

# Make this script idempotent
rm -rf $WORKDIR
# cifuzz.bash will use this directory
rm -rf /tmp/cifuzz
mkdir $WORKDIR

#git clone https://skia.googlesource.com/skia.git --depth 1 $WORKDIR/skia
cp -r ~/cifuzz/cifuzz-external-example $WORKDIR/cifuzz-external-example

export PROJECT_SRC_PATH=$WORKDIR/cifuzz-external-example
CIFUZZ_SCRIPT_PATH=$PWD/cifuzz.bash

pushd $WORKDIR

# pushd skia
# ./bin/sync
# pushd third_party/externals/swiftshader/
# git checkout 45510ad8a77862c1ce2e33f0efed41544f5f048b
# popd
# popd

echo "hi"
BUILD_INTEGRATION_PATH=fuzzer-build-integration OSS_FUZZ_PROJECT_NAME=cifuzz-external-example REPO_NAME=cifuzz-external-example COMMIT_SHA=649a76622006a32bff3f2d3b800e811738947971 $CIFUZZ_SCRIPT_PATH
